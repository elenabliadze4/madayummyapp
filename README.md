Hi, we want to offer you a new application, MadaYummyApp. This application gives you opportunity to buy cakes much more easily, meaning that there will be no need for you to leave the house!
In order to use the app you only need to sign up with your e-mail, after that you can view cakes and then choose it to your heart’s content!
The app is also enjoyable to use since you can have some fun choosing your profile picture. And last but not least, 
the cherry on top of this app: you can collect scores on your card and later on you can buy a cake with these scores!.
The application is connected to the realtimedatabase of firebase, meaning that the account will not be deleted when you leave the app.
the app still needs to be perfected and our company, B&B, tries to correct those glitches. And we will offer you a newer and naturally better version of the app.

Respectfully,
B&B